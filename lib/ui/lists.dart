import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:todo_lists/model/todoList.dart';
import 'package:todo_lists/model/task_object.dart';
import 'package:todo_lists/ui/create_todos.dart';
import 'package:todo_lists/ui/detail_page.dart';
import 'package:todo_lists/util/color_choices.dart';
import 'package:todo_lists/util/create_list.dart';
// import 'package:todo_lists/util/custom_icons.dart';
import 'package:todo_lists/util/custom_scroll_physics.dart';
import 'package:todo_lists/util/database_helper.dart';
import 'package:todo_lists/util/menu_options.dart';
import 'package:http/http.dart' as http;

class Lists extends StatefulWidget {
  @override
  _ListsState createState() => new _ListsState();
}

class _ListsState extends State<Lists> {
  ScrollController scrollController;
  Color backgroundColor;
  Tween<Color> colorTween;
  int currentPage = 0;
  Color constBackColor;
  List<TodoList> items = new List();
  DatabaseHelper db = new DatabaseHelper();
  CustomScrollPhysics physicsX;
  String _timeString;
  String _quote = "";

  @override
  void initState() {
    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    // fetchQuote();
    super.initState();
    db.getAllNotes().then((notes) {
      setState(() {
        notes.forEach((note) {
          TodoList tl = TodoList.fromMap(note);
          List<TaskObject> toList = new List();
          // toList.add(TaskObject('test', new DateTime.now(), 1));
          db.getAllTasks(tl.id).then((tasks) {
            List<TaskObject> toList = new List();
            tasks.forEach((task) {
              // print(task);
              toList.add(TaskObject.fromMap(task));
            });
            tl.tasks = toList;
          });
          tl.tasks = toList;
          items.add(tl);
        });
        setState(() {
          physicsX = new CustomScrollPhysics.basic(items, false);

          if (items.length == 0) {
            colorTween = new ColorTween(begin: ColorChoies.colors[0]["color"], end: ColorChoies.colors[0]["color"]);
            backgroundColor = ColorChoies.colors[2]["color"];
          } else {
            Color color1 = new Color(items[0].color);
            Color color2 = items.length > 1 ? new Color(items[1].color) : new Color(items[0].color);
            colorTween = new ColorTween(begin: color1, end: color2);
            backgroundColor = new Color(items[0].color);
          }
        });
      });
    });

    scrollController = new ScrollController();

    scrollController.addListener(() {
      ScrollPosition position = scrollController.position;
      ScrollDirection direction = position.userScrollDirection;
      int page = (position.pixels / (position.maxScrollExtent / (items.length.toDouble() - 1))).toInt();
      double pageDo = (position.pixels / (position.maxScrollExtent / (items.length.toDouble() - 1)));
      double percent = pageDo - page;
//      print("int page: " + page.toString());
//      print("double page: " + pageDo.toString());
//      print("percent " + percent.toString());
      if (direction == ScrollDirection.reverse) {
        //page begin
        if (items.length - 1 < page + 1) {
          return;
        }
        colorTween.begin = new Color(items[page].color);
        colorTween.end = new Color(items[page + 1].color);
        setState(() {
          backgroundColor = colorTween.lerp(percent);
        });
      } else if (direction == ScrollDirection.forward) {
        //+1 begin page end
        if (items.length - 1 < page + 1) {
          return;
        }
        colorTween.begin = new Color(items[page].color);
        colorTween.end = new Color(items[page + 1].color);
        setState(() {
          backgroundColor = colorTween.lerp(percent);
        });
      } else {
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    {
      final double _width = MediaQuery.of(context).size.width;
      // final double _ratioW = _width / 375.0;

      final double _height = MediaQuery.of(context).size.height;
      // final double _ratioH = _height / 812.0;

      return new Container(
        decoration: new BoxDecoration(color: backgroundColor),
        child: new Scaffold(
            resizeToAvoidBottomPadding: false,
            backgroundColor: Colors.transparent,
            appBar: new AppBar(
              backgroundColor: Colors.transparent,
//              automaticallyImplyLeading: false,
              elevation: 0.0,
              title: new Text("EASY LISTS"),
              // actions: <Widget>[
              //   new IconButton(
              //     icon: new Icon(
              //       CustomIcons.search,
              //       size: 26.0,
              //     ),
              //     onPressed: () {},
              //   )
              // ],
            ),
            body: new Container(
              child: new Stack(
                children: <Widget>[
                  new Container(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          padding: const EdgeInsets.only(top: 20.0, bottom: 0.0, left: 50.0, right: 60.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              // new Padding(
                              //   padding: const EdgeInsets.only(bottom: 25.0),
                              //   child: new Container(
                              //     decoration: new BoxDecoration(
                              //       boxShadow: [new BoxShadow(color: Colors.black38, offset: new Offset(5.0, 5.0), blurRadius: 15.0)],
                              //       shape: BoxShape.circle,
                              //     ),
                              //     child: new CircleAvatar(
                              //       backgroundColor: Colors.grey,
                              //     ),
                              //   ),
                              // ),
                              new Padding(
                                padding: const EdgeInsets.only(bottom: 10.0),
                                child: new Text(
                                  "$_timeString",
                                  style: new TextStyle(color: Colors.white, fontSize: 25.0),
                                ),
                              ),
                              new Text(
                                _quote,
                                style: new TextStyle(color: Colors.white70),
                              ),
                              // new Text(
                              //   "You have 10 tasks to do today.",
                              //   style: new TextStyle(color: Colors.white70),
                              // ),
                            ],
                          ),
                        ),
                        new Container(
                          height: 320.0,
                          width: _width,
                          child: items.length == 0
                              ? Center(
                                  child: Text(
                                    "WAITING FOR YOUR FIRST LIST :)",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                  ),
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    TodoList todoObject = items[index];
                                    EdgeInsets padding = const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 30.0);

                                    double percentComplete = todoObject.percentComplete();

                                    return new Padding(
                                        padding: padding,
                                        child: new InkWell(
                                          onTap: () {
                                            Navigator.of(context).push(new PageRouteBuilder(
                                                pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => new DetailPage(todoObject: todoObject),
                                                transitionsBuilder: (
                                                  BuildContext context,
                                                  Animation<double> animation,
                                                  Animation<double> secondaryAnimation,
                                                  Widget child,
                                                ) {
                                                  return new SlideTransition(
                                                    position: new Tween<Offset>(
                                                      begin: const Offset(0.0, 1.0),
                                                      end: Offset.zero,
                                                    ).animate(animation),
                                                    child: new SlideTransition(
                                                      position: new Tween<Offset>(
                                                        begin: Offset.zero,
                                                        end: const Offset(0.0, 1.0),
                                                      ).animate(secondaryAnimation),
                                                      child: child,
                                                    ),
                                                  );
                                                },
                                                transitionDuration: const Duration(milliseconds: 1000)));
                                          },
                                          child: new Container(
                                              decoration: new BoxDecoration(borderRadius: new BorderRadius.circular(10.0), boxShadow: [new BoxShadow(color: Colors.black.withAlpha(70), offset: const Offset(3.0, 10.0), blurRadius: 15.0)]),
                                              height: 250.0,
                                              child: new Stack(
                                                children: <Widget>[
                                                  new Hero(
                                                    tag: todoObject.uuid + "_background",
                                                    child: new Container(
                                                      decoration: new BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius: new BorderRadius.circular(10.0),
                                                      ),
                                                    ),
                                                  ),
                                                  new Padding(
                                                    padding: const EdgeInsets.all(16.0),
                                                    child: new Column(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisSize: MainAxisSize.max,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        new Expanded(
                                                          child: new Row(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Hero(
                                                                tag: todoObject.uuid + "_icon",
                                                                child: new Container(
                                                                  decoration: new BoxDecoration(
                                                                    shape: BoxShape.circle,
                                                                    border: new Border.all(color: Colors.grey.withAlpha(70), style: BorderStyle.solid, width: 1.0),
                                                                  ),
                                                                  child: new Padding(
                                                                    padding: const EdgeInsets.all(8.0),
                                                                    child: new Icon(todoObject.type, color: new Color(todoObject.color)),
                                                                  ),
                                                                ),
                                                              ),
                                                              new Expanded(
                                                                child: new Container(
                                                                    alignment: Alignment.topRight,
                                                                    child: new Hero(
                                                                      tag: todoObject.uuid + "_more_vert",
                                                                      child: new Material(
                                                                        color: Colors.transparent,
                                                                        type: MaterialType.transparency,
                                                                        child: new PopupMenuButton<String>(
                                                                            onSelected: (value) {
                                                                              if (value == 'Edit') {
                                                                                Navigator.of(context)
                                                                                    .push(
                                                                                  new MaterialPageRoute<List<dynamic>>(builder: (context) => new CreateList.update(todoObject)),
                                                                                )
                                                                                    .then((List<dynamic> value) {
                                                                                  if (value == null) {
                                                                                    return;
                                                                                  }
                                                                                  var order;
                                                                                  if (value[0]["state"] == 'back') {
                                                                                    order = false;
                                                                                  } else if (value[0]["state"] == 'update') {
                                                                                    order = false;
                                                                                    var index = items.indexOf(value[2]["todoOld"]);
                                                                                    value[1]["todoNew"].tasks = value[2]["todoOld"].tasks;
                                                                                    setState(() {
                                                                                      items[index] = value[1]["todoNew"];
                                                                                      backgroundColor = Color(value[1]["todoNew"].color);
                                                                                    });
                                                                                  } else {
                                                                                    order = true;
                                                                                  }
                                                                                });
                                                                              } else {
                                                                                _neverSatisfied(todoObject, index);
                                                                              }
                                                                            },
                                                                            itemBuilder: (BuildContext context) => MenuOptions.choices.map((String choice) {
                                                                                  return PopupMenuItem<String>(
                                                                                    value: choice,
                                                                                    child: Text(choice),
                                                                                  );
                                                                                }).toList()
                                                                            // items: MenuOptions.choices.map((String choice) {
                                                                            //   return DropdownMenuItem<String>(
                                                                            //     value: choice,
                                                                            //     child: Text(choice),
                                                                            //   );
                                                                            // }).toList(),
                                                                            ),
                                                                      ),
                                                                    )),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        new Padding(
                                                            padding: const EdgeInsets.only(bottom: 8.0),
                                                            child: new Align(
                                                                alignment: Alignment.bottomLeft,
                                                                child: new Hero(
                                                                  tag: todoObject.uuid + "_number_of_tasks",
                                                                  child: new Material(
                                                                      color: Colors.transparent,
                                                                      child: new Text(
                                                                        todoObject.taskAmount().toString() + " Tasks",
                                                                        softWrap: false,
                                                                        style: new TextStyle(),
                                                                      )),
                                                                ))),
                                                        new Padding(
                                                          padding: const EdgeInsets.only(bottom: 20.0),
                                                          child: new Align(
                                                              alignment: Alignment.bottomLeft,
                                                              child: new Hero(
                                                                tag: todoObject.uuid + "_title",
                                                                child: new Material(
                                                                  color: Colors.transparent,
                                                                  child: new Text(
                                                                    todoObject.title.toUpperCase(),
                                                                    softWrap: false,
                                                                    maxLines: 4,
                                                                    overflow: TextOverflow.fade,
                                                                    style: new TextStyle(fontSize: 45.0),
                                                                  ),
                                                                ),
                                                              )),
                                                        ),
                                                        new Align(
                                                            alignment: Alignment.bottomLeft,
                                                            child: new Hero(
                                                                tag: todoObject.uuid + "_progress_bar",
                                                                child: new Material(
                                                                  color: Colors.transparent,
                                                                  child: new Row(
                                                                    children: <Widget>[
                                                                      new Expanded(
                                                                        child: new LinearProgressIndicator(
                                                                          value: percentComplete,
                                                                          backgroundColor: Colors.grey.withAlpha(50),
                                                                          valueColor: new AlwaysStoppedAnimation<Color>(new Color(todoObject.color)),
                                                                        ),
                                                                      ),
                                                                      new Padding(
                                                                        padding: const EdgeInsets.only(left: 5.0),
                                                                        child: new Text((percentComplete * 100).round().toString() + "%"),
                                                                      )
                                                                    ],
                                                                  ),
                                                                )))
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )),
                                        ));
                                  },
                                  padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                                  scrollDirection: Axis.horizontal,
                                  physics: physicsX,
                                  controller: scrollController,
                                  itemExtent: _width - 80,
                                  itemCount: items.length,
                                ),
                        )
                      ],
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(right: 15.0, bottom: 15.0),
                    child: new Align(
                        alignment: Alignment.bottomRight,
                        child: new FloatingActionButton(
                          onPressed: () {
                            Navigator.of(context)
                                .push(
                              new MaterialPageRoute<List<dynamic>>(builder: (context) => new CreateList.normal()),
                            )
                                .then((List<dynamic> value) {
                              if (value == null) {
                                return;
                              }
                              print('vine');
                              print(value[0]["state"]);
                              var order;
                              if (value[0]["state"] == 'back') {
                                order = false;
                              } else if (value[0]["state"] == 'new') {
                                order = true;
                                setState(() {
                                  List<TaskObject> toList = new List();
                                  value[1]["todo"].tasks = toList;
                                  items.insert(0, value[1]["todo"]);
                                  // backgroundColor = Color(value[1]["todo"].color);
                                });
                              } else {
                                order = true;
                              }
                              // physicsX = new CustomScrollPhysics.basic(items, order);
                              // db.getAllNotes().then((notes) {
                              //   setState(() {
                              //     items = [];
                              //     notes.forEach((note) {
                              //       items.add(TodoList.fromMap(note));
                              //     });
                              //     physicsX = new CustomScrollPhysics.basic(items, order);
                              //   });
                              // });
                            });
                          },
                          tooltip: 'Add a new List',
                          child: new Icon(Icons.add),
                        )),
                  )
                ],
              ),
            )),
      );
    }
  }

  Future<void> _neverSatisfied(TodoList note, int position) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete List'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure?'),
                // Text('You\’re like me. I’m never satisfied.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              color: Color(ColorChoies.blue),
              textColor: Color(ColorChoies.white),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Delete'),
              onPressed: () {
                _deleteNote(context, note, position);
              },
            )
          ],
        );
      },
    );
  }

  IconButton buildIconButton() {
    return new IconButton(
      icon: new Icon(
        Icons.more_vert,
        color: Colors.grey,
      ),
      onPressed: () {
        return;
      },
    );
  }
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      title: 'JSA ListView Demo',
//      home: Scaffold(
//        appBar: AppBar(
//          title: Text('Lists'),
//          centerTitle: true,
//          backgroundColor: Colors.blue,
//        ),
//        body: Center(
//          child: ListView.builder(
//              itemCount: items.length,
//              padding: const EdgeInsets.all(15.0),
//              itemBuilder: (context, position) {
//                return Column(
//                  children: <Widget>[
//                    Divider(height: 5.0),
//                    ListTile(
//                      title: Text(
//                        '${items[position].title}',
//                        style: TextStyle(
//                          fontSize: 22.0,
//                          color: Colors.deepOrangeAccent,
//                        ),
//                      ),
//                      subtitle: Text(
//                        '${items[position].description}',
//                        style: new TextStyle(
//                          fontSize: 18.0,
//                          fontStyle: FontStyle.italic,
//                        ),
//                      ),
//                      leading: Column(
//                        children: <Widget>[
//                          Padding(padding: EdgeInsets.all(10.0)),
//                          CircleAvatar(
//                            backgroundColor: Colors.blueAccent,
//                            radius: 15.0,
//                            child: Text(
//                              '${items[position].id}',
//                              style: TextStyle(
//                                fontSize: 22.0,
//                                color: Colors.white,
//                              ),
//                            ),
//                          ),
//                          IconButton(
//                              icon: const Icon(Icons.remove_circle_outline),
//                              onPressed: () => _deleteNote(context, items[position], position)),
//                        ],
//                      ),
//                      onTap: () => _navigateToNote(context, items[position]),
//                    ),
//                  ],
//                );
//              }),
//        ),
//        floatingActionButton: FloatingActionButton(
//          child: Icon(Icons.add),
//          onPressed: () => _createNewNote(context),
//        ),
//      ),
//    );
//  }

  // void _createNewNote(BuildContext context) async {
  //   String result = await Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => CreateTodos(TodoList('', 'Work', 'blue'))),
  //   );

  //   if (result == 'save') {
  //     db.getAllNotes().then((notes) {
  //       setState(() {
  //         items.clear();
  //         notes.forEach((note) {
  //           items.add(TodoList.fromMap(note));
  //         });
  //       });
  //     });
  //   }
  // }

  void _deleteNote(BuildContext context, TodoList note, int position) async {
    db.deleteNote(note.id).then((notes) {
      setState(() {
        items.removeAt(position);
      });
      Navigator.of(context).pop();
      print(position + 1);
      print(items.length);
      if (items.length == 0) {
        backgroundColor = ColorChoies.colors[2]["color"];
      } else if (position + 1 > items.length) {
        backgroundColor = new Color(items[position - 1].color);
      } else if (position + 1 == 1) {
        backgroundColor = new Color(items[position].color);
      } else if (position + 1 < items.length) {
        backgroundColor = new Color(items[position].color);
      } else if (position + 1 == items.length) {
        backgroundColor = new Color(items[position].color);
      }
      // else if (position == 0 && items.length == 1) {
      //   // backgroundColor = ColorChoies.colors[2]["color"];
      //   backgroundColor = new Color(items[0].color);
      // } else if (position == 0 && items.length > 1) {
      //   backgroundColor = new Color(items[position].color);
      // } else {
      //   backgroundColor = new Color(items[position + 1].color);
      // }
      // backgroundColor = (items.length > 0) ? new Color(items[position - 1].color): ColorChoies.colors[2]["color"];
    });
  }

  // void _navigateToNote(BuildContext context, TodoList note) async {
  //   String result = await Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => CreateTodos(note)),
  //   );

  //   if (result == 'update') {
  //     db.getAllNotes().then((notes) {
  //       setState(() {
  //         items.clear();
  //         notes.forEach((note) {
  //           items.add(TodoList.fromMap(note));
  //         });
  //       });
  //     });
  //   }
  // }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    // return DateFormat('MMMM d hh:mm:ss').format(dateTime);
    return DateFormat('MMMM d').format(dateTime);
  }

  Future<http.Response> fetchQuote() {
    return http.get('https://random-math-quote-api.herokuapp.com/').then((res) {
      print(res.body);
      Map<String, dynamic> randomQuote = json.decode(res.body);
      print(randomQuote["quote"]);
      _quote = randomQuote["quote"];
    });
  }
}
