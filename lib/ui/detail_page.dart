import 'package:flutter/material.dart';
import 'package:todo_lists/model/task_object.dart';
import 'package:todo_lists/model/todoList.dart';
import 'package:intl/intl.dart';
import 'package:todo_lists/util/create_list.dart';
import 'package:todo_lists/util/custom_checkbox.dart';
import 'package:todo_lists/util/database_helper.dart';
import 'package:todo_lists/util/menu_options.dart';

class DetailPage extends StatefulWidget {
  DetailPage({@required this.todoObject, Key key}) : super(key: key);

  TodoList todoObject;

  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage> with TickerProviderStateMixin {
  double percentComplete;
  AnimationController animationBar;
  double barPercent = 0.0;
  Tween<double> animT;
  AnimationController scaleAnimation;
  bool enter = false;
  final myController = TextEditingController();
  DatabaseHelper db = new DatabaseHelper();

  @override
  void initState() {
    scaleAnimation = new AnimationController(vsync: this, duration: const Duration(milliseconds: 1000), lowerBound: 0.0, upperBound: 1.0);

    percentComplete = widget.todoObject.percentComplete();
    barPercent = percentComplete;
    animationBar = new AnimationController(vsync: this, duration: const Duration(milliseconds: 100))
      ..addListener(() {
        setState(() {
          barPercent = animT.lerp(animationBar.value);
        });
      });
    animT = new Tween<double>(begin: percentComplete, end: percentComplete);
    scaleAnimation.forward().whenComplete(() {
      setState(() {
        this.enter = true;
      });
    });

    super.initState();
    // Start listening to changes
    myController.addListener(_printLatestValue);
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    myController.dispose();
    super.dispose();
  }

  void updateBarPercent() async {
    double newPercentComplete = widget.todoObject.percentComplete();
    if (animationBar.status == AnimationStatus.forward || animationBar.status == AnimationStatus.completed) {
      animT.begin = newPercentComplete;
      await animationBar.reverse();
    } else if (animationBar.status == AnimationStatus.reverse || animationBar.status == AnimationStatus.dismissed) {
      animT.end = newPercentComplete;
      await animationBar.forward();
    } else {
      // print("wtf");
    }
    percentComplete = newPercentComplete;
  }

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Hero(
          tag: widget.todoObject.uuid + "_background",
          child: new Container(
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.circular(0.0),
            ),
          ),
        ),
        WillPopScope(
          onWillPop: (() {
            setState(() {
              this.enter = false;
            });
            Navigator.of(context).pop();
          }),
          child: new Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomPadding: false,
            appBar: new AppBar(
              iconTheme: new IconThemeData(color: Colors.black),
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              leading: new IconButton(
                icon: new Icon(
                  Icons.arrow_back,
                  color: this.enter ? Colors.grey : Colors.transparent,
                ),
                onPressed: () {
                  setState(() {
                    this.enter = false;
                  });
                  Navigator.of(context).pop();
                },
              ),
              actions: <Widget>[
                new Hero(
                  tag: widget.todoObject.uuid + "_more_vert",
                  child: new Material(
                    color: Colors.white,
                    type: MaterialType.transparency,
                    child: new PopupMenuButton<String>(
                      itemBuilder: (BuildContext context) => MenuOptions.choices.map((String choice) {
                            return PopupMenuItem<String>(
                              value: choice,
                              child: Text(choice),
                            );
                          }).toList(),
                      onSelected: (value) {
                        if (value == 'Edit') {
                          Navigator.of(context)
                              .push(
                            new MaterialPageRoute<List<dynamic>>(builder: (context) => new CreateList.update(widget.todoObject)),
                          )
                              .then((List<dynamic> value) {
                            if (value == null) {
                              return;
                            }
                            var uuid = widget.todoObject.uuid;
                            // var order;
                            if (value[0]["state"] == 'back') {
                              // order = false;
                            } else if (value[0]["state"] == 'update') {
                              // order = false;
                              // var index = items.indexOf(value[2]["todoOld"]);
                              value[1]["todoNew"].tasks = value[2]["todoOld"].tasks;
                              value[1]["todoNew"].uuid = uuid;
                              setState(() {
                                widget.todoObject = value[1]["todoNew"];
                                print(widget.todoObject.title.toString());
                                // backgroundColor = Color(value[1]["todoNew"].color);
                              });
                            } else {
                              // order = true;
                            }
                          });
                        }
                      },
                    ),
                  ),
                )
              ],
            ),
            body: new Padding(
              padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 35.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.only(bottom: 30.0),
                    child: new Align(
                      alignment: Alignment.bottomLeft,
                      child: new Hero(
                        tag: widget.todoObject.uuid + "_icon",
                        child: new Container(
                          decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            border: new Border.all(color: Colors.grey.withAlpha(70), style: BorderStyle.solid, width: 1.0),
                          ),
                          child: new Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Icon(
                              widget.todoObject.type,
                              color: new Color(widget.todoObject.color),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                      padding: const EdgeInsets.only(bottom: 12.0),
                      child: new Align(
                          alignment: Alignment.bottomLeft,
                          child: new Hero(
                            tag: widget.todoObject.uuid + "_number_of_tasks",
                            child: new Material(
                                color: Colors.transparent,
                                child: new Text(
                                  widget.todoObject.taskAmount().toString() + " Tasks",
                                  softWrap: false,
                                  style: new TextStyle(),
                                )),
                          ))),
                  new Container(
                    width: 400.0,
                    child: new Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: new Align(
                          alignment: Alignment.bottomLeft,
                          child: new Hero(
                            tag: widget.todoObject.uuid + "_title",
                            child: new Material(
                              color: Colors.transparent,
                              child: new Text(
                                widget.todoObject.title.toUpperCase(),
                                softWrap: true,
                                maxLines: 6,
                                overflow: TextOverflow.fade,
                                style: new TextStyle(fontSize: 30.0),
                              ),
                            ),
                          )),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(bottom: 30.0),
                    child: new Align(
                        alignment: Alignment.bottomLeft,
                        child: new Hero(
                            tag: widget.todoObject.uuid + "_progress_bar",
                            child: new Material(
                              color: Colors.transparent,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    child: new LinearProgressIndicator(
                                      value: barPercent,
                                      backgroundColor: Colors.grey.withAlpha(50),
                                      valueColor: new AlwaysStoppedAnimation<Color>(new Color(widget.todoObject.color)),
                                    ),
                                  ),
                                  new Padding(
                                    padding: const EdgeInsets.only(left: 5.0),
                                    child: new Text((barPercent * 100).round().toString() + "%"),
                                  )
                                ],
                              ),
                            ))),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(bottom: 30.0),
                    child: TextField(
                      controller: myController,
                      onSubmitted: (value) {
                        DateTime now = DateTime.now();
                        String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(now);
                        TaskObject task = new TaskObject(value, formattedDate, widget.todoObject.id);
                        db.saveTask(task).then((res) {
                          task.id = res;
                          setState(() {
                            widget.todoObject.tasks.insert(0, task);
                            updateBarPercent();
                          });
                          myController.clear();
                        });
                      },
                    ),
                  ),
                  new Expanded(
                      child: new ScaleTransition(
                    scale: scaleAnimation,
                    child: new ListView.builder(
                      padding: const EdgeInsets.all(0.0),
                      itemBuilder: (BuildContext context, int index) {
                        // DateTime currentDate = widget.todoObject.tasks.keys.toList()[index];
                        // DateTime currentDate = DateTime(2019);
                        // DateTime _now = new DateTime.now();
                        // DateTime today = new DateTime(_now.year, _now.month, _now.day);
                        // String dateString;
                        // if (currentDate.isBefore(today)) {
                        //   dateString = "Previous - " + new DateFormat.E().format(currentDate);
                        // } else if (currentDate.isAtSameMomentAs(today)) {
                        //   dateString = "Today";
                        // } else if (currentDate.isAtSameMomentAs(today.add(const Duration(days: 1)))) {
                        //   dateString = "Tomorrow";
                        // } else {
                        //   dateString = new DateFormat.E().format(currentDate);
                        // }
                        // List<Widget> tasks = [new Text(dateString)];
                        List<Widget> tasks = [];
                        widget.todoObject.tasks.forEach((task) {
                          tasks.add(new CustomCheckboxListTile(
                            activeColor: new Color(widget.todoObject.color),
                            value: task.isCompleted(),
                            onChanged: (value) {
                              setState(() {
                                task.setComplete(value);
                                updateBarPercent();
                                db.updateTask(task);
                              });
                            },
                            title: new Text(task.task),
                            // secondary: new Icon(Icons.alarm),
                          ));
                        });
                        return new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: tasks,
                        );
                      },
                      // itemCount: widget.todoObject.tasks.length,
                      itemCount: 1,
                    ),
                  ))
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  _printLatestValue() {
    // print("Second text field: ${myController.text}");
  }
}
