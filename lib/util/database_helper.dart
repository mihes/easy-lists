import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:todo_lists/model/todoList.dart';
import 'package:todo_lists/model/task_object.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  final String tableNote = 'lists';
  final String columnId = 'id';
  final String columnType = 'type';
  final String columnTitle = 'title';
  final String columnDescription = 'description';
  final String columnColor = 'color';
  final String columnTasks = 'tasks';
  final String columnUuid = 'uuid';

  final String tableTask = 'tasks';
  final String columnTaskId = 'id';
  final String columnTaskTask = 'task';
  final String columnTaskListId = 'list_id';
  final String columnTaskDate = 'date';
  final String columnTaskCompleted = 'completed';

  static Database _db;

  DatabaseHelper.internal();

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();

    return _db;
  }

  initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'todos.db');

    // await deleteDatabase(path); // just for testing

    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $tableNote($columnId INTEGER PRIMARY KEY, $columnTitle TEXT, $columnDescription TEXT, $columnColor INTEGER,  $columnUuid TEXT, $columnType TEXT ) ');
    await db.execute('CREATE TABLE $tableTask($columnTaskId INTEGER PRIMARY KEY, $columnTaskListId INTEGER, $columnTaskTask TEXT, $columnTaskDate TEXT, $columnTaskCompleted INTEGER, FOREIGN KEY($columnTaskListId) REFERENCES $tableNote($columnId) )');
    // TaskObject task = new TaskObject("Meet Clients", new DateTime(2018, 5, 3), 1);
    // var map = new Map<String, dynamic>();
    // map['task'] = task.task;
    // map['date'] = task.date;
    // map['list_id'] = task.listId;

    // db.insert(tableTask, map);
  }

  Future<int> saveNote(TodoList note) async {
    var dbClient = await db;
    var result = await dbClient.insert(tableNote, note.toMap());
//    var result = await dbClient.rawInsert(
//        'INSERT INTO $tableNote ($columnTitle, $columnDescription) VALUES (\'${note.title}\', \'${note.description}\')');
    // print(result);
    return result;
  }

  Future<int> saveTask(TaskObject task) async {
    var dbClient = await db;
    var result = await dbClient.insert(tableTask, task.toMap());
//    var result = await dbClient.rawInsert(
//        'INSERT INTO $tableNote ($columnTitle, $columnDescription) VALUES (\'${note.title}\', \'${note.description}\')');

    return result;
  }

  Future<List> getAllNotes() async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(
      tableNote,
      columns: [columnId, columnTitle, columnDescription, columnColor, columnType, columnUuid],
      orderBy: "$columnId DESC",
    );

    return result.toList();
  }

  Future<List> getAllTasks(id) async {
    var dbClient = await db;
    var res = await dbClient.query(tableTask, columns: [columnTaskId, columnTaskTask, columnTaskDate, columnTaskCompleted, columnTaskListId], orderBy: "$columnTaskId DESC", where: '$columnTaskListId = ?', whereArgs: [id]);
    return res.toList();
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM $tableNote'));
  }

  Future<TodoList> getNote(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(tableNote, columns: [columnId, columnTitle, columnDescription], where: '$columnId = ?', whereArgs: [id]);
//    var result = await dbClient.rawQuery('SELECT * FROM $tableNote WHERE $columnId = $id');

    if (result.length > 0) {
      return new TodoList.fromMap(result.first);
    }

    return null;
  }

  Future<TaskObject> getTask(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(tableTask, columns: [columnTaskId, columnTaskTask, columnTaskDate, columnTaskCompleted, columnTaskListId], where: '$columnTaskId = ?', whereArgs: [id]);
//    var result = await dbClient.rawQuery('SELECT * FROM $tableNote WHERE $columnId = $id');

    if (result.length > 0) {
      return new TaskObject.fromMap(result.first);
    }

    return null;
  }

  Future<int> deleteNote(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableNote, where: '$columnId = ?', whereArgs: [id]).then((res) {
      deleteChildTasks(id);
    });
//    return await dbClient.rawDelete('DELETE FROM $tableNote WHERE $columnId = $id');
  }

  Future<int> deleteChildTasks(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableTask, where: '$columnTaskListId = ?', whereArgs: [id]);
//    return await dbClient.rawDelete('DELETE FROM $tableNote WHERE $columnId = $id');
  }

  Future<int> updateNote(TodoList note) async {
    var dbClient = await db;
    return await dbClient.update(tableNote, note.toMap(), where: "$columnId = ?", whereArgs: [note.id]);
//    return await dbClient.rawUpdate(
//        'UPDATE $tableNote SET $columnTitle = \'${note.title}\', $columnDescription = \'${note.description}\' WHERE $columnId = ${note.id}');
  }

  Future<int> updateTask(TaskObject task) async {
    var dbClient = await db;
    return await dbClient.update(tableTask, task.toMap(), where: "$columnTaskId = ?", whereArgs: [task.id]);
//    return await dbClient.rawUpdate(
//        'UPDATE $tableNote SET $columnTitle = \'${note.title}\', $columnDescription = \'${note.description}\' WHERE $columnId = ${note.id}');
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
