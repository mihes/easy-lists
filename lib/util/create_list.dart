import 'package:flutter/material.dart';
import 'package:todo_lists/model/todoList.dart';
import 'package:todo_lists/ui/lists.dart';
import 'package:todo_lists/util/color_choices.dart';
import 'package:todo_lists/util/database_helper.dart';

//import 'CustomAppBar.dart';

class CreateList extends StatefulWidget {
  TodoList todo;
  CreateList.normal();
  CreateList.update(this.todo);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _CreateForm();
  }
}

class _CreateForm extends State<CreateList> {
  TodoList todo;

  @override
  void initState() {
    super.initState();

    todo = widget.todo;
  }
  // CreateList({todo: this.todo});

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Easy List',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Scaffold(
        backgroundColor: Color(0xFF5A89E6),
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: new Text("CREATE LIST"),
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                List data = new List();
                data.add({"state": "back"});
                Navigator.of(context).pop(data);
//                  Navigator.of(context).pop(
//                    new MaterialPageRoute(
//                        builder: (context) => new Lists()),
//                  );
              }),
        ),
        body: new ListForm(
          title: 'Create list',
          todo: this.todo,
          originalContext: context,
        ),
      ),
    );
//    Scaffold(
//      appBar: AppBar(
//        title: Text("Create List"),
//      ),
//      body: Container(
//          child: ListForm()
//      ),
//      backgroundColor: Color(0xFF5A89E6),
//
//    );
  }
}

// Define a Custom Form Widget
class ListForm extends StatefulWidget {
  ListForm({Key key, this.title, this.todo, this.originalContext}) : super(key: key);
  final String title;
  final TodoList todo;
  BuildContext originalContext;

  @override
  ListFormState createState() {
    return ListFormState();
  }
}

// Define a corresponding State class. This class will hold the data related to
// the form.
class ListFormState extends State<ListForm> {
//  var _img = new Image.network(
//      "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/TUCPamplona10.svg/500px-TUCPamplona10.svg.png");
  String _title = '';
  String _selected = '';
  String _color;
  _LoginData data = new _LoginData();
  TextEditingController _titleController;
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a `GlobalKey<FormState>`, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _titleController = (widget.todo != null) ? new TextEditingController(text: widget.todo.title) : new TextEditingController(text: '');
    if (widget.todo != null) {
      //  var res = typeList.where((list) => list["icon"] == widget.todo.type);
      _selected = widget.todo.type2;
    } else {
      _selected = 'Work';
    }
    if (widget.todo != null) {
      var res = ColorChoies.colors.where((list) => list["color"] == Color(widget.todo.color));
      _color = res.elementAt(0)["name"];
    } else {
      _color = ColorChoies.colors[1]["name"];
    }
    // _selected =  (widget.todo != null) ? widget.todo.type :'Work';

    super.initState();
  }

//  tipo.add([
//  {"icon": Icons.person, "type": "Personal"},
//  {"icon": IFcons.work, "type": "Work"},
//  {"icon": Icons.home, "type": "Home"},
//  {"icon": Icons.shopping_cart, "type": "Shopping"},
//  {"icon": Icons.school, "type": "School"}
//  ]);
  @override
  Widget build(BuildContext context) {
    List<Map<dynamic, dynamic>> typeList = [];

    typeList.add({"icon": Icons.person, "type": "Personal"});
    typeList.add({"icon": Icons.work, "type": "Work"});
    typeList.add({"icon": Icons.home, "type": "Home"});
    typeList.add({"icon": Icons.shopping_cart, "type": "Shopping"});
    typeList.add({"icon": Icons.school, "type": "School"});
//    selected = typeList[1]['type'];
    final Size screenSize = MediaQuery.of(context).size;
    _LoginData _data = new _LoginData();

    // Build a Form widget using the _formKey we created above
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: new Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0xFF5A89E6),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 32.0),
          child: Form(
              key: _formKey,
              child: Card(
                margin: const EdgeInsets.symmetric(vertical: 32.0),
                child: Container(
                  width: screenSize.width,
                  height: screenSize.height / 2,
                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                          controller: _titleController,
                          decoration: new InputDecoration(labelText: 'List Title'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter Some Text';
                            }
                          },
                          onSaved: (String value) {
                            _data.title = value;
                          }),
                      new Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Row(
                          children: <Widget>[new Text("Type")],
                        ),
                      ),
                      new DropdownButton<String>(
                        isExpanded: true,
                        value: _selected,
                        hint: Text('Icon'),
                        items: typeList.map((Map<dynamic, dynamic> value) {
                          return new DropdownMenuItem<String>(
                            value: value['type'],
//                    child: new Text(value['type']),
                            child: new Container(
                              padding: const EdgeInsets.only(bottom: 5.0, right: 10.0),
                              height: 100.0,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Icon(value['icon']),
                                  new Container(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    height: 100.0,
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[new Text(value['type'])],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _selected = value;
                            _data.type = _selected;
                          });
                        },
                      ),
                      new Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Row(
                          children: <Widget>[new Text("Color")],
                        ),
                      ),
                      new DropdownButton<String>(
                        isExpanded: true,
                        value: _color,
                        hint: Text('Color'),
                        items: ColorChoies.colors.map((Map<String, dynamic> value) {
                          return new DropdownMenuItem<String>(
                            value: value['name'],
//                    child: new Text(value['type']),
                            child: new Container(
                              padding: const EdgeInsets.only(bottom: 5.0, right: 10.0),
                              height: 100.0,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                                    color: value['color'],
                                    width: 48.0,
                                    height: 48.0,
                                  ),
                                ],
                              ),
                            ),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _color = value;
//                    _data.type = selected;
                          });
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6.0),
                        child: Builder(
                          builder: (context) => RaisedButton(
                                onPressed: () {
                                  // Validate will return true if the form is valid, or false if
                                  // the form is invalid.
                                  if (_formKey.currentState.validate()) {
                                    // If the form is valid, we want to show a Snackbar

                                    _formKey.currentState.save(); // Save our form now.
                                    var dbHelper = DatabaseHelper();
                                    // Scaffold.of(context).showSnackBar(
                                    //     SnackBar(content: Text('Data Saved')));
                                    if (widget.todo == null) {
                                      TodoList todoObj = new TodoList(_data.title, _selected, _color);
                                      dbHelper.saveNote(todoObj).then((res) {
                                        todoObj.id = res;
                                        List data = new List();
                                        data.add({"state": "new"});
                                        data.add({"todo": todoObj});
                                        Navigator.of(widget.originalContext).pop(data);
                                      });
                                    } else {
                                      TodoList todoObj = new TodoList(_data.title, _selected, _color);
                                      todoObj.id = widget.todo.id;
                                      dbHelper.updateNote(todoObj).then((res) {
                                        List data = new List();
                                        data.add({"state": "update"});
                                        data.add({"todoNew": todoObj});
                                        data.add({"todoOld": widget.todo});
                                        Navigator.of(widget.originalContext).pop(data);
                                        // Navigator.of(context).pushReplacement(
                                        //   new MaterialPageRoute<String>(builder: (context) => new Lists()),
                                        // );
                                      });
                                    }

//                                Navigator.of(context).pop('saved');
//                                dbHelper.getAllNotes();
                                  }
                                },
                                child: (widget.todo != null) ? Text('Update') : Text('Add'),
                              ),
                        ),
                      ),
                    ],
                  ),
                ),
              )),
        ),
      ),
    );
  }
}

class _LoginData {
  String title = '';
  String type = '';
  String color = '';
}
