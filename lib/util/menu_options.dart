class MenuOptions {
  static const String edit = 'Edit';
  static const String delete = 'Delete';

  static const List<String> choices = <String> [
    edit,
    delete
  ];
}