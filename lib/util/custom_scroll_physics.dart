import 'package:flutter/cupertino.dart';
import 'package:todo_lists/model/todoList.dart';

class CustomScrollPhysics extends ScrollPhysics {
  List<TodoList> todosItems;
  double numOfItems;
  bool starting = true;

  CustomScrollPhysics({Key: Key, ScrollPhysics parent, this.todosItems, this.starting}) : super(parent: parent);

  CustomScrollPhysics.basic(List<TodoList> todosItems, starting) {
    this.todosItems = todosItems;
    this.numOfItems = todosItems.length.toDouble() - 1;
    this.starting = starting;
  }

  @override
  CustomScrollPhysics applyTo(ScrollPhysics ancestor) {
    return new CustomScrollPhysics(parent: buildParent(ancestor), todosItems: todosItems, starting: starting);
  }

  double _getPage(ScrollPosition position) {
    this.numOfItems = todosItems.length.toDouble() - 1;
    return position.pixels / (position.maxScrollExtent / numOfItems);
    // return position.pixels / position.viewportDimension;
  }

  double _getPixels(ScrollPosition position, double page) {
    if (this.starting) {
      page = -1.0;
    }
    this.starting = false;
    this.numOfItems = todosItems.length.toDouble() - 1;
    // return page * position.viewportDimension;
    return page * (position.maxScrollExtent / numOfItems);
  }

  double _getTargetPixels(ScrollPosition position, Tolerance tolerance, double velocity) {
    double page = _getPage(position);
    if (velocity < -tolerance.velocity)
      page -= 0.5;
    else if (velocity > tolerance.velocity) page += 0.5;
    return _getPixels(position, page.roundToDouble());
  }

  @override
  Simulation createBallisticSimulation(ScrollMetrics position, double velocity) {
    if ((velocity <= 0.0 && position.pixels <= position.minScrollExtent) || (velocity >= 0.0 && position.pixels >= position.maxScrollExtent)) return super.createBallisticSimulation(position, velocity);
    final Tolerance tolerance = this.tolerance;
    final double target = _getTargetPixels(position, tolerance, velocity);
    if (target != position.pixels) return new ScrollSpringSimulation(spring, position.pixels, target, velocity, tolerance: tolerance);
    return null;
  }
}
