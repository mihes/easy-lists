import 'package:flutter/material.dart';

class ColorChoies {
  static const blue = 0xFF5A89E6;
  static const red = 0xFFD50000;
  static const green = 0xFF4EC5AC;
  static const white = 0xFFFFFFFF;
  static const black = 0xFF000000;
  static const purple = 0xFF311b92;
  static const yellow = 0xFFff6f00;
  static const orange = 0xFFF77B67;

//  static const List<Color> colors = [
//    const Color(0xFF5A89E6),
//    const Color(0xFFF77B67),
//    const Color(0xFF4EC5AC),
//  ];

  static const List<Map<String, dynamic>> colors = [
    {"color": Color(blue), "name": "blue"},
    {"color": Color(red), "name": "red"},
    {"color": Color(green), "name": "green"},
    {"color": Color(purple), "name": "purple"},
    {"color": Color(yellow), "name": "yellow"},
    {"color": Color(orange), "name": "orange"},
  ];
}
