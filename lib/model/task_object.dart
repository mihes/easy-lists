class TaskObject {
  int id;
  String date;
  String task;
  int listId;
  bool _completed;

  TaskObject(String task, String date, int listId) {
    this.task = task;
    this.date = date;
    this._completed = false;
    this.listId = listId;
  }
  

  TaskObject.import(String task, String date, bool completed) {
    this.task = task;
    this.date = date;
    this._completed = completed;
    
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['task'] = task;
    map['date'] = date;
    map['list_id'] = listId;
    map['completed'] = _completed;

    return map;
  }

  TaskObject.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.task = map['task'];
    this.date = map['date'];
    this.listId = map['list_id'];
    this._completed =  map['completed'] == 1? true:false;
  }

  void setComplete(bool value) {
    _completed = value;
  }

  isCompleted() => _completed;

  // isEmpty() => TaskObject.fromJson().isEmpty();

}