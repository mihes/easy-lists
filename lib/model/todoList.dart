import 'package:flutter/material.dart';
import 'package:todo_lists/model/task_object.dart';
import 'package:todo_lists/util/color_choices.dart';
import 'package:uuid/uuid.dart';

class TodoList {
  int _id;
  String _title;
  String _description;
  int _color;
  String _type;
  String _uuid;
  // Map<DateTime, List<TaskObject>> _tasks;User user;
  List<TaskObject> _tasks;

  TodoList(String title, String type, String color) {
    this._title = title;
    this._type = type;
    this._description = 'description';
    if (color == 'red') {
      this._color = ColorChoies.red;
    } else if (color == 'blue') {
      this._color = ColorChoies.blue;
    } else if (color == 'purple') {
      this._color = ColorChoies.purple;
    } else if (color == 'yellow') {
      this._color = ColorChoies.yellow;
    } else if (color == 'orange') {
      this._color = ColorChoies.orange;
    } else {
      this._color = ColorChoies.green;
    }

    this._uuid = new Uuid().v1();
    // this._tasks =  List<TaskObject>();

    // this.tasks
  }

  TodoList.map(dynamic obj) {
    this._id = obj['id'];
    this._title = obj['title'];
    this._color = obj['color'];
    this._description = obj['description'];
    this._type = obj['type'];
    this._uuid = obj['uuid'];
  }

  void set id(int id) {
    _id = id;
  }

  void set tasks(List<TaskObject> tasks) {
    _tasks = tasks;
  }

  void set uuid(String uuid) {
    _uuid = uuid;
  }

  int get id => _id;
  String get title => _title;
  String get description => _description;
  int get color => _color;
  String get uuid => _uuid;
  IconData get type {
    if (this._type == 'Work') {
      return Icons.work;
    } else if (this._type == 'Personal') {
      return Icons.person;
    } else if (this._type == 'Home') {
      return Icons.home;
    } else if (this._type == 'Shopping') {
      return Icons.shopping_cart;
    } else {
      return Icons.school;
    }
  }

  String get type2 => _type;
  List<TaskObject> get tasks => _tasks;

  double percentComplete() {
    if (tasks == null) {
      return 0.0;
    }

    if (tasks.isEmpty) {
      return 0.0;
    }
    int completed = 0;
    int amount = 0;
    amount += tasks.length;
    tasks.forEach((task) {
      if (task.isCompleted()) {
        completed++;
      }
    });
    // tasks.forEach((list) {
    //   amount += list.length;
    //   list.forEach((task) {
    //     if (task.isCompleted()) {
    //       completed++;
    //     }
    //   });
    // });
    return completed / amount;
  }

  int taskAmount() {
    int amount = 0;
    if (tasks == null) {
      return amount;
    }
    amount += tasks.length;
    return amount;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['title'] = _title;
    map['type'] = _type;
    map['description'] = _description;
    map['color'] = _color;
    // map['tasks'] = _tasks;
    map['uuid'] = _uuid;

    return map;
  }

  TodoList.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._title = map['title'];
    this._type = map['type'];
    this._description = map['description'];
    this._color = map['color'];
    // this._tasks = map['tasks'];
    this._uuid = map['uuid'];
  }
}
